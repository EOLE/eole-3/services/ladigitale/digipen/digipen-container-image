# Digipen

Digipen est un éditeur simple de code HTML, CSS et JS basé sur Flems (https://github.com/porsager/flems).

Il est publié sous licence GNU AGPLv3.
Sauf la fonte Source Code Pro (Apache License Version 2.0), la fonte HKGrotesk (Sil Open Font Licence 1.1) et le fichier flems.html (WTFPL licence)

### Démo
https://ladigitale.dev/digipen/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/
